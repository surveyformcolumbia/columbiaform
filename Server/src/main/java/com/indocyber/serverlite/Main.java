package com.indocyber.serverlite;

import com.indocyber.serverlite.pages.*;

import static spark.Spark.*;
/**
 * Created by Indocyber on 07/07/2017.
 */
public class Main {
    public static void main(String[] args) throws ClassNotFoundException {
        System.out.println("Server Running on 0.0.0.0:4567");
        staticFiles.location("/public");
        /*
        ========page list =================
         */
        path("/admin", () -> {
            before("/*",(q,a)->{
               if(q.session().isNew()){
                   a.redirect("/");
               }
            });
            get("/",Admin::manageUser);
            get("/device/get_all",Devices::getAll);
            post("/device/save_all",Devices::saveAll);
            get("/user/get_all",Users::getAll);
            get("/log/get_all",LogUsers::getLogAll);
            get("/log/",Admin::listLog);
            put("/user/add",Users::Add);
            put("/user/update",Users::Update);
            post("/user/delete",Users::Delete);
        });
        path("/logout", () -> {
            before("/*",(q,a)->{
               q.session().invalidate();
            });
            after("/*",(q,a)->{
                a.redirect("/");
            });
            get("/", Admin::login);
        });
        get("/", Admin::login);
        post("/login", Admin::loginProcess);
        post("/user/login", Users::Login);
        post("/user/change_settings", Users::changeSettings);
        put("/customer/add", Customers::Add);
        put("/customer/update", Customers::Update);
        delete("/customer/delete", Customers::Delete);
        get("/customer/get_all", Customers::getAll);
        put("/customer/add_log", LogUsers::Add);
        get("/sync/get_object", Sync::getObject);
        get("/sync/all", Sync::ObjectLists);
        /*
        ==========error list ==================
         */
        internalServerError(Errors::error500);
        notFound(Errors::error404);
    }
}

package com.indocyber.serverlite.lib;

import org.json.JSONArray;
import org.json.JSONObject;

import java.sql.*;

/**
 * Created by Indocyber on 07/07/2017.
 */
public class QueryManager {
    public Configuration configuration=new Configuration();
    private JSONObject responseData;
    private final String username="alan";
    private final String password="qq123";
    private final String url="jdbc:sqlserver://127.0.0.1:1433;DatabaseName=Test";
    private final String className="com.microsoft.sqlserver.jdbc.SQLServerDriver";
    private Connection connection=null;
    public QueryManager(){
        connection = null;
        try
        {
            Class.forName(className);
            /*
            open connection data
             */
            connection = DriverManager.getConnection(url,username, password);
        }
        catch (ClassNotFoundException e)
        {
            e.printStackTrace();
            System.exit(1);
        }
        catch (SQLException e)
        {
            e.printStackTrace();
            System.exit(2);
        }
    }
    public JSONObject getDataByQuery(String query){
        /*
        create response
         */
        responseData = new JSONObject();
        try {
            Statement command=connection.createStatement();
            if(configuration.databaseLog) {
                System.out.println("========Query Select========");
                System.out.println(query);
            }
            ResultSet resultSet=command.executeQuery(query);
            ResultSetMetaData resultSetMetaData=resultSet.getMetaData();
            /*
            store result to json array
             */
            JSONArray result=new JSONArray();
            while (resultSet.next()){
                JSONObject json=new JSONObject();
                /*
                column mapping to json data
                 */
                for(int i=1;i<=resultSetMetaData.getColumnCount();i++){
                    json.put(resultSetMetaData.getColumnName(i),resultSet.getString(resultSetMetaData.getColumnName(i)));
                }
                result.put(json);
            }
            /*
            put json data result
             */
            responseData.put("status",true);
            responseData.put("data",result);
            responseData.put("count",result.length());
            connection.close();
        } catch (SQLException e) {
            /*
            put json error
             */
            if(configuration.databaseLog) {
                System.out.println(e.getMessage().toString());
            }
            responseData.put("status",false);
            responseData.put("message","Database Server Error");
            try {
                connection.close();
            } catch (SQLException e1) {
                responseData.put("status", false);
                responseData.put("message", "Connection Not Closed");
            }
        }
        return responseData;
    }
    public JSONObject crudQuery(String query) {
          /*
        create response
         */
        responseData = new JSONObject();
        try {
            Statement command = connection.createStatement();
            if(configuration.databaseLog) {
                System.out.println("===========Query Update, Insert & Delete========");
                System.out.println(query);
            }
            command.executeUpdate(query);
            responseData.put("status", true);
            responseData.put("message", "Operation Success");
            connection.close();
        } catch (SQLException e) {
            /*
            put json error
             */
            if(configuration.databaseLog) {
                System.out.println(e.getMessage().toString());
            }
            responseData.put("status", false);
            responseData.put("message", "Database Server Error");
            try {
                connection.close();
            } catch (SQLException e1) {
                responseData.put("status", false);
                responseData.put("message", "Connection Not Closed");
            }
        }
        return responseData;
    }
}

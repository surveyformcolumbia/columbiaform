package com.indocyber.serverlite.lib;

import spark.Request;

/**
 * Created by Indocyber on 12/07/2017.
 */
public class Logger {
    public Configuration configuration=new Configuration();
    public void printLog(Request request) {
        if (configuration.logEnable) {
            System.out.println("Client " + request.ip()+" "+request.userAgent()+" Request " + request.requestMethod() + " " + request.uri());
        }
    }
}

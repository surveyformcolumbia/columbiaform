package com.indocyber.serverlite.pages;

import com.indocyber.serverlite.lib.Logger;
import com.indocyber.serverlite.lib.QueryManager;
import org.json.JSONArray;
import org.json.JSONObject;
import spark.Request;
import spark.Response;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by Indocyber on 11/07/2017.
 */
public class LogUsers {
    /*
    method add logusers
   */
    public static String Add(Request request , Response response){
        new Logger().printLog(request);
        response.type("application/json");
         /*
       get parameters
         */
        String user_id=request.queryParams("user_id");
        String customer_id=request.queryParams("customer_id");
        String latlong=request.queryParams("latlong");
        String image=request.queryParams("image");
        SimpleDateFormat dateFormat=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Date date=new Date();
        /*
        create query manager
         */
        QueryManager db=new QueryManager();
        String InsertQuery="INSERT INTO LogUsers (UserID,CustomerID,LatLong,LoginDate,Image)" +
                "VALUES" +
                "('"+user_id+"','"+customer_id+"','"+latlong+"','"+dateFormat.format(date)+"','"+image+"')";
        JSONObject insert=db.crudQuery(InsertQuery);
        JSONObject json=new JSONObject();
        /*
        if success
         */
        if(insert.getBoolean("status")){
            json.put("status",true);
            json.put("message","Log Saved");
        }else{
            json=insert;
        }
        return json.toString();
    }
    /*
    method get All Logs
     */
    public static JSONObject getLogAll(Request request, Response response) {
        new Logger().printLog(request);
        response.type("application/json");
        response.header("Cache-Control","no-cache");
        /*
        create query manager
         */
        QueryManager db=new QueryManager();
        String SelectQuery="SELECT LoginDate, Username , Fullname, Address, LatLong,Image From LogLists ORDER BY LoginDate DESC";
        JSONObject users=db.getDataByQuery(SelectQuery);
        JSONObject json=new JSONObject();
        if(users.getBoolean("status")){
            JSONArray data=users.getJSONArray("data");
            JSONArray parsedTable=new JSONArray();
            for (int i=0;i<data.length();i++){
                JSONArray temp=new JSONArray();
                temp.put(data.getJSONObject(i).getString("LoginDate"));
                temp.put(data.getJSONObject(i).getString( "Username"));
                temp.put(data.getJSONObject(i).getString("Fullname"));
                temp.put(data.getJSONObject(i).getString("Address"));
                temp.put(data.getJSONObject(i).getString("LatLong"));
                temp.put("<img src='data:image/jpeg;base64,"+data.getJSONObject(i).getString("Image")+"'" +
                        " width='75'/>");
                parsedTable.put(i,temp);
            }
            json.put("data",parsedTable);
        }else{
            JSONArray parsedTable=new JSONArray();
            json.put("data",parsedTable);
        }
        return json;
    }
}

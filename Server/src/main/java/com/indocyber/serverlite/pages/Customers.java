package com.indocyber.serverlite.pages;

import com.indocyber.serverlite.lib.Logger;
import com.indocyber.serverlite.lib.QueryManager;
import org.json.JSONObject;
import spark.Request;
import spark.Response;

/**
 * Created by Indocyber on 10/07/2017.
 */
public class Customers extends Page {
    /*
   method add customers
    */
    public static String Add(Request request , Response response){
        new Logger().printLog(request);
        response.type("application/json");
         /*
        get parameters
         */
        String fullname=request.queryParams("Fullname");
        String email=request.queryParams("Email");
        String nohp=request.queryParams("NoHP");
        String address=request.queryParams("Address");
        String gender=request.queryParams("Gender");
        /*
        create query manager
         */
        QueryManager db=new QueryManager();
        String InsertQuery="INSERT INTO Customers (Fullname , Email , NoHP, Address , Gender)" +
                "VALUES" +
                "('"+fullname+"','"+email+"','"+nohp+"','"+address+"','"+gender+"')";
        JSONObject insert=db.crudQuery(InsertQuery);
        JSONObject json=new JSONObject();
        /*
        if success
         */
        if(insert.getBoolean("status")){
            json.put("status",true);
            json.put("message","Customer Saved");
        }else{
            json=insert;
        }
        return json.toString();
    }
    /*
    method update customers
   */
    public static String Update(Request request , Response response){
        new Logger().printLog(request);
        response.type("application/json");
        /*
        get parameters
         */
        String customer_id=request.queryParams("CustomerID");
        String fullname=request.queryParams("Fullname");
        String email=request.queryParams("Email");
        String nohp=request.queryParams("NoHP");
        String address=request.queryParams("Address");
        String gender=request.queryParams("Gender");
        /*
        create query manager
         */
        QueryManager db=new QueryManager();
        String updateQuery="UPDATE Customers SET Fullname='"+fullname+"' , Email='"+email+"', " +
                "NoHP='"+nohp+"' , Address='"+address+"' , Gender='"+gender+"'" +
                " WHERE CustomerID='"+customer_id+"'";
        JSONObject update =db.crudQuery(updateQuery);
        JSONObject json=new JSONObject();
        /*
        if success
         */
        if(update.getBoolean("status")){
            json.put("status",true);
            json.put("message","Customer Has been Deleted");
        }else{
            json=update;
        }
        return json.toString();
    }
    /*
        method update customers
        */
    public static String Delete(Request request , Response response){
        new Logger().printLog(request);
        response.type("application/json");
         /*
        get parameters
         */
        String customer_id=request.queryParams("CustomerID");
        /*
        create query manager
         */
        QueryManager db=new QueryManager();
        String InsertQuery="DELETE FROM Customers WHERE CustomerID='"+customer_id+"'";
        JSONObject delete=db.crudQuery(InsertQuery);
        JSONObject json=new JSONObject();
        /*
        if success
         */
        if(delete.getBoolean("status")){
            json.put("status",true);
            json.put("message","Customer Has been Deleted");
        }else{
            json=delete;
        }
        return json.toString();
    }
    /*
    method get all customer
     */
    public static String getAll(Request request , Response response){
        new Logger().printLog(request);
        response.type("application/json");
        /*
        create query manager
         */
        QueryManager db=new QueryManager();
        String SelectQuery="SELECT CustomerID, Fullname, Email , Address, NoHP , Gender FROM Customers";
        JSONObject customers=db.getDataByQuery(SelectQuery);
        JSONObject json=new JSONObject();
        if(customers.getBoolean("status")){
            json.put("status",true);
            /*
            data encrypter
             */
            json.put("data",customers.getJSONArray("data"));
        }else{
            json.put("status",true);
            json.put("message","No Result Found");
        }
        return json.toString();
    }
}

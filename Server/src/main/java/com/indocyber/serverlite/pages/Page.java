package com.indocyber.serverlite.pages;

import com.indocyber.serverlite.lib.Logger;
import spark.ModelAndView;
import spark.template.velocity.VelocityTemplateEngine;

import java.util.Map;

/**
 * Created by Indocyber on 11/07/2017.
 */
public class Page {
    // declare this in a util-class
    public static String render(Map<String, Object> model, String templatePath) {
        return new VelocityTemplateEngine().render(new ModelAndView(model, "views/"+templatePath));
    }
}

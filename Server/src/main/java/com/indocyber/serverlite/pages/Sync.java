package com.indocyber.serverlite.pages;

import com.indocyber.serverlite.lib.Logger;
import com.indocyber.serverlite.lib.QueryManager;
import org.json.JSONArray;
import org.json.JSONObject;
import spark.Request;
import spark.Response;

/**
 * Created by Indocyber on 17/07/2017.
 */
public class Sync {
    /*
    method add logusers
   */
    public static JSONObject getObject(Request request , Response response) {
        new Logger().printLog(request);
        response.type("application/json");
        /*
        post method body get
         */
        String token = request.queryParams("token");
        String object = request.queryParams("object");
        JSONObject json=new JSONObject();
        /*
        sync Object All
         */
        QueryManager db=new QueryManager();
        String SelectQuery="SELECT UserID from UserTokens Where Token='"+token+"'";
        JSONObject selectUser=db.getDataByQuery(SelectQuery);
        if(selectUser.getInt("count") > 0){
            /*
            get object
             */
            String SelectObject="";
            if(object.equals("MOBILEORDER")){
                SelectObject="SELECT * from "+object+" WHERE USER_ID='"+selectUser.getJSONArray("data").getJSONObject(0).getString("UserID") +"'";
            }else{
                SelectObject="SELECT * from "+object;
            }
            QueryManager dbObject=new QueryManager();
            JSONObject resulttObject=dbObject.getDataByQuery(SelectObject);
            if(resulttObject.getBoolean("status")){
                json.put("status", true);
                json.put("data", resulttObject.getJSONArray("data"));
                json.put("message", "There's Result");
            }else{
                json.put("status", false);
                json.put("message", "No Result");
            }
        }else{
            /*
            api failed
            */
            json.put("status", false);
            json.put("message", "Invalid Token");
        }
        return json;
    }
    /*
    method add logusers
   */
    public static JSONObject ObjectLists(Request request , Response response) {
        new Logger().printLog(request);
        response.type("application/json");
        /*
        post method body get
         */
        String token = request.queryParams("token");
        JSONObject json=new JSONObject();
        /*
        sync Object All
         */
        QueryManager db=new QueryManager();
        String SelectQuery="SELECT UserID from UserTokens Where Token='"+token+"'";
        JSONObject selectUser=db.getDataByQuery(SelectQuery);
        if(selectUser.getInt("count") > 0){
            JSONArray objectList=new JSONArray();
            objectList.put(0,"COMBOBOX");
            objectList.put(1,"CUSTOMER_JOB_TITLE_MST");
            objectList.put(2,"MOBILEORDER");
            objectList.put(3,"OCCUPATION_MST");
            objectList.put(4,"OTHER_FINANCING_TYPE_MST");
            objectList.put(5,"PRODUCT_CLASSIFICATION_MST");
            objectList.put(6,"PRODUCT_CONDITION_MST");
            objectList.put(7,"PROVINCE_MST");
            objectList.put(8,"REFERENCE_RELATION_MST");
            objectList.put(9,"RELIGION_MST");
            objectList.put(10,"STATUS_KUNJUNGAN_MST");
            objectList.put(11,"ZIP");
            /*
            api success
            */
            json.put("status", true);
            json.put("data", objectList);
            json.put("message", "There's Result");
        }else{
            /*
            api failed
            */
            json.put("status", false);
            json.put("message", "Invalid Token");
        }
        return json;
    }
}

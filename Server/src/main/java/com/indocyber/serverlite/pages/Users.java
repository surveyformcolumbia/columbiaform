package com.indocyber.serverlite.pages;

import com.indocyber.serverlite.lib.Logger;
import com.indocyber.serverlite.lib.QueryManager;
import org.json.JSONArray;
import org.json.JSONObject;
import spark.Request;
import spark.Response;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.UUID;

/**
 * Created by Indocyber on 08/07/2017.
 */
public class Users extends Page {
    /*
    method login user
     */
    public static String Login(Request request, Response response) {
        new Logger().printLog(request);
        response.type("application/json");
      /*
        post method body get
         */
        String device_id = request.queryParams("device_id");
        String username = request.queryParams("username");
        String password = request.queryParams("password");
       /*
       create query manager
        */
        QueryManager loginManager = new QueryManager();
        /*
         exec login query
         */
        String loginQuery = "SELECT UserID,Username,Fullname, Address, DeviceID FROM LoginUsers " +
                "WHERE DeviceID='"+device_id+"' AND Username ='" + username + "' AND password='" + password + "' AND Role='Surveyor'";
        JSONObject result = loginManager.getDataByQuery(loginQuery);
        /*
        if login success
         */
        JSONObject json = new JSONObject();
        if (result.getInt("count") > 0) {
            /*
            login success
             */
            //get system date
            Date date=new Date();
            SimpleDateFormat dateFormat=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            StringBuffer sb = new StringBuffer();
            String tokenKey="";
            try {
                tokenKey=dateFormat.format(date)+"-"+username;
                MessageDigest md5=MessageDigest.getInstance("MD5");
                md5.update(tokenKey.getBytes());
                byte[] digest = md5.digest();
                for (byte b : digest) {
                    sb.append(String.format("%02x", b & 0xff));
                }
                /*
               create query manager
                */
                String UserID=result.getJSONArray("data").getJSONObject(0).getString("UserID");
                QueryManager insertToken = new QueryManager();
                JSONObject insertQuery=insertToken.crudQuery("INSERT INTO Tokens(Token,TokenDate,UserID)" +
                        "VALUES" +
                        "('"+sb.toString()+"','"+dateFormat.format(date)+"','"+UserID+"')");
                if(insertQuery.getBoolean("status")){
                    json.put("status", true);
                    json.put("token", sb.toString());
                    json.put("data", result.getJSONArray("data"));
                    json.put("message", "Login Success");
                }else{
                    json.put("status", false);
                    json.put("message", "Failed to Create Token");
                }
            } catch (NoSuchAlgorithmException e) {
              System.out.println("Server Error Access Token");
                json.put("status", false);
                json.put("message", "Token Error");
            }
        } else {
            /*
            login failed
             */
            json.put("status", false);
            json.put("message", "Invalid Username/Password");
        }
        return json.toString();
    }

    /*
    method change user information
     */
    public static String changeSettings(Request request, Response response) {
        new Logger().printLog(request);
        JSONObject json = new JSONObject();
        response.type("application/json");
        String token = request.queryParams("token");
        /*
       create query manager
        */
        QueryManager loginManager = new QueryManager();
        /*
         exec check token query
         */
        String tokenQuery = "SELECT UserID FROM UserTokens WHERE Token='"+token+"'";
        JSONObject resultToken = loginManager.getDataByQuery(tokenQuery);
        if(resultToken.getInt("count") > 0) {
            String user_id = resultToken.getJSONArray("data").getJSONObject(0).getString("UserID");
            String username = request.queryParams("username");
            String fullname = request.queryParams("fullname");
            String address = request.queryParams("address");
            String opassword = request.queryParams("opassword");
            String npassword = request.queryParams("npassword");
            /*
            if new password nulled
             */
            QueryManager changeInfo = new QueryManager();
            String updateQuery = "";
            if (npassword.equals("")) {
                updateQuery = "UPDATE Users SET Username='" + username + "',  Fullname='" + fullname + "', Address='" + address + "' " +
                        "Where UserID='" + user_id + "'";
            } else {
            /*
            check old password
             */
                if (opassword.equals("")) {
                    json.put("status", false);
                    json.put("message", "Enter Old Password");
                    return json.toString();
                } else {
                /*
                  create query manager
                */
                    QueryManager checkPassword = new QueryManager();
                    String queryPassword = "SELECT Username FROM Users WHERE UserID='" + user_id + "' AND password='" + opassword + "'";
                    JSONObject result = checkPassword.getDataByQuery(queryPassword);
                    if (result.getInt("count") < 1) {
                        json.put("status", false);
                        json.put("message", "Invalid Old Password");
                        return json.toString();
                    } else {
                        updateQuery = "UPDATE Users SET Username='" + username + "',  Fullname='" + fullname + "', Address='" + address + "' , Passsword='" + npassword + "'" +
                                "Where UserID='" + user_id + "'";
                    }
                }
            }
            JSONObject update = changeInfo.crudQuery(updateQuery);
            if (update.getBoolean("status")) {
                json.put("status", true);
                json.put("message", "User Information Saved");
            } else {
                json.put("status", false);
                json.put("message", "Failed to Save Changes");
            }
        }else{
            json.put("status", false);
            json.put("message", "Invalid Token");
        }
        return json.toString();
    }

    /*
    method get All Users
     */
    public static JSONObject getAll(Request request, Response response) {
        new Logger().printLog(request);
        response.type("application/json");
        response.header("Cache-Control","no-cache");
        /*
        create query manager
         */
        QueryManager db=new QueryManager();
        String SelectQuery="SELECT UserID, Username, Fullname , Address FROM Users Where Role='Surveyor'";
        JSONObject users=db.getDataByQuery(SelectQuery);
        JSONObject json=new JSONObject();
        if(users.getBoolean("status")){
            JSONArray data=users.getJSONArray("data");
            JSONArray parsedTable=new JSONArray();
            /*
            generate data table data list
             */
           for (int i=0;i<data.length();i++){
               JSONArray temp=new JSONArray();
               temp.put(data.getJSONObject(i).getString("UserID"));
               temp.put(data.getJSONObject(i).getString( "Username"));
               temp.put(data.getJSONObject(i).getString("Fullname"));
               temp.put(data.getJSONObject(i).getString("Address"));
               String btnManage="<button data-body='"+data.getJSONObject(i).toString()+"' data-userid=\""+data.getJSONObject(i).getString("UserID")+"\"  type=\"button\" class=\"btn btn-warning btn-md devices\">Devices</button>";
               String btnEdit="<button data-body='"+data.getJSONObject(i).toString()+"' data-userid=\""+data.getJSONObject(i).getString("UserID")+"\"  type=\"button\" class=\"btn btn-primary btn-md edit\">Edit</button>";
               String btnDelete="<button data-body='" + data.getJSONObject(i).toString() + "' data-userid=\"" + data.getJSONObject(i).getString("UserID") + "\" type=\"button\" class=\"btn btn-danger btn-md delete\">Delete</button>";
               temp.put(btnManage+" "+btnEdit+" "+btnDelete);
               parsedTable.put(i,temp);
           }
           json.put("data",parsedTable);
        }else{
            JSONArray parsedTable=new JSONArray();
            json.put("data",parsedTable);
        }
        return json;
    }
    /*
    method delete user
     */
    public static JSONObject Delete(Request request, Response response) {
        new Logger().printLog(request);
        response.type("application/json");
        /*
        post method body get
         */
        String user_id = request.queryParams("user_id");
       /*
       create query manager
        */
        QueryManager queryManager = new QueryManager();
        /*
         exec delete query
         */
        String deleteQuery = "DELETE FROM Users Where UserID='"+user_id+"'";
        JSONObject result = queryManager.crudQuery(deleteQuery);
        /*
        if login success
         */
        JSONObject json = new JSONObject();
        if (result.getBoolean("status")) {
            /*
            login success
             */
            json.put("status", true);
            json.put("message", "User Deleted");
        } else {
            /*
            login failed
             */
            json.put("status", false);
            json.put("message", "Failed to Delete User");
        }
        return json;
    }
    /*
     method update user
    */
    public static JSONObject Update(Request request, Response response) {
        new Logger().printLog(request);
        response.type("application/json");
        String user_id = request.queryParams("user_id");
        String username = request.queryParams("username");
        String fullname = request.queryParams("fullname");
        String address = request.queryParams("address");
        String password = request.queryParams("password");
        /*
        if new password nulled
         */
        QueryManager changeInfo = new QueryManager();
        String updateQuery = "";
        JSONObject json = new JSONObject();
        if (password.equals("")) {
            updateQuery = "UPDATE Users SET Username='" + username + "',  Fullname='" + fullname + "', Address='" + address + "' " +
                    "Where UserID='" + user_id + "'";
        } else {
            updateQuery = "UPDATE Users SET Username='" + username + "',  Fullname='" + fullname + "', Address='" + address + "' , Password='"+password +"'" +
                    "Where UserID='" + user_id + "'";
        }
        JSONObject update = changeInfo.crudQuery(updateQuery);
        if (update.getBoolean("status")) {
            json.put("status", true);
            json.put("message", "User Updated");
        } else {
            json.put("status", false);
            json.put("message", "Failed to User");
        }
        return json;
    }
    /*
     method add user
    */
    public static JSONObject Add(Request request, Response response) {
        new Logger().printLog(request);
        response.type("application/json");
        /*
        post method body get
         */
        String username = request.queryParams("username");
        String password = request.queryParams("password");
        String fullname = request.queryParams("fullname");
        String address = request.queryParams("address");
       /*
       create query manager
        */
        QueryManager queryManager = new QueryManager();
        /*
         exec insert query
         */
        UUID ID= UUID.randomUUID();
        String IDs=ID.toString();
        String[] strings=IDs.split("-");
        String insertQuery = "INSERT INTO Users (UserID, Username, Fullname, Password , Address, Role)" +
                "VALUES" +
                "('"+strings[0]+"','"+username+"','"+fullname+"','"+password+"','"+address+"','Surveyor')";
        JSONObject result = queryManager.crudQuery(insertQuery);
        /*
        if login success
         */
        JSONObject json = new JSONObject();
        if (result.getBoolean("status")) {
            /*
            login success
             */
            json.put("status", true);
            json.put("message", "User Added");
        } else {
            /*
            login failed
             */
            json.put("status", false);
            json.put("message", "Failed to Insert User");
        }
        return json;
    }
}
package com.indocyber.serverlite.pages;

import com.indocyber.serverlite.lib.Logger;
import com.indocyber.serverlite.lib.QueryManager;
import org.json.JSONObject;
import spark.Request;
import spark.Response;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by Indocyber on 11/07/2017.
 */
public class Admin extends Page {
    /*
   method login user
    */
    public static String login(Request request , Response response){
        new Logger().printLog(request);
        Map<String, Object> model = new HashMap<>();
        return render(model,"login.vm");
    }
    /*
     method login process
   */
    public static JSONObject loginProcess(Request request , Response response){
        new Logger().printLog(request);
        response.type("application/json");
        /*
         post method body get
        */
        String username=request.queryParams("username");
        String password=request.queryParams("password");
        /*
        create query manager
        */
        QueryManager loginManager=new QueryManager();
        /*
         exec login query
         */
        String loginQuery="SELECT UserID,Username,Fullname, Address FROM Users " +
                "WHERE Username ='"+username+"' AND password='"+password+"' AND Role='Admin'";
        JSONObject result=loginManager.getDataByQuery(loginQuery);
        /*
        if login success
         */
        JSONObject json=new JSONObject();
        if(result.getInt("count")>0){
            /*
            login success and set sesssion
            */
            request.session(true);
            request.session().attribute("username",username);
            request.session().attribute("userdata",result.getJSONArray("data").getJSONObject(0).toString());
            json.put("status",true);
            json.put("message","Login Success");
        }else{
                /*
                login failed
                 */
            json.put("status",false);
            json.put("message","Invalid Username/Password/DeviceID");
        }
        return json;
    }
    /*
      method manage user
       */
    public static String manageUser(Request request , Response response){
        new Logger().printLog(request);
        JSONObject userdata=new JSONObject(request.session().attribute("userdata").toString());
        Map<String, Object> model = new HashMap<>();
        model.put("username",userdata.getString("Username"));
        model.put("fullname",userdata.getString("Fullname"));
        return render(model,"manage_users.vm");
    }
    /*
      method log user
       */
    public static String listLog(Request request , Response response){
        new Logger().printLog(request);
        JSONObject userdata=new JSONObject(request.session().attribute("userdata").toString());
        Map<String, Object> model = new HashMap<>();
        model.put("username",userdata.getString("Username"));
        model.put("fullname",userdata.getString("Fullname"));
        return render(model,"log_lists.vm");
    }
}

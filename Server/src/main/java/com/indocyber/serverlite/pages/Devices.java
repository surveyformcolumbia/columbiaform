package com.indocyber.serverlite.pages;

import com.indocyber.serverlite.lib.Logger;
import com.indocyber.serverlite.lib.QueryManager;
import org.json.JSONArray;
import org.json.JSONObject;
import spark.Request;
import spark.Response;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by Indocyber on 18/07/2017.
 */
public class Devices extends Page {
    /*
    method check device
     */
    public static JSONObject checkDevice(Request request, Response response) {
        response.type("application/json");
        JSONObject json = new JSONObject();
        return  json;
    }
    /*
    method get all devices
     */
    public static JSONObject getAll(Request request, Response response) {
        new Logger().printLog(request);
        response.type("application/json");
        response.header("Cache-Control","no-cache");
        String user_id = request.queryParams("user_id");
        /*
        create query manager
         */
        QueryManager db=new QueryManager();
        String SelectQuery="SELECT DeviceID FROM Devices WHERE UserID='"+user_id+"'";
        JSONObject devices=db.getDataByQuery(SelectQuery);
        JSONObject json=new JSONObject();
        if(devices.getBoolean("status")){
            json.put("data",devices.getJSONArray("data"));
        }else{
            JSONArray parsedTable=new JSONArray();
            json.put("data",parsedTable);
        }
        return json;
    }
    /*
    method get save all device
     */
    public static JSONObject saveAll(Request request, Response response) {
        response.type("application/json");
        String user_id=request.queryParams("user_id");
        String devices=request.queryParams("device");
        String[] deviceList=devices.split(",");
        /*
        create query manager
         */
        QueryManager db=new QueryManager();
        String deleteQuery="DELETE FROM Devices WHERE UserID='"+user_id+"'";
        JSONObject deleteDevice=db.crudQuery(deleteQuery);
        JSONObject json=new JSONObject();
        if(deleteDevice.getBoolean("status")){
            /*
            add All Device
             */
            SimpleDateFormat dateFormat=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            Date date=new Date();
            for (int i=0;i<deviceList.length;i++){
                QueryManager insertDb=new QueryManager();
                String insertQuery="INSERT INTO Devices (DeviceID,InsertedDate,UserID)" +
                        "VALUES" +
                        "('"+deviceList[i]+"','"+dateFormat.format(date)+"','"+user_id+"')";
                insertDb.crudQuery(insertQuery);
            }
            System.out.println(devices);
            json.put("status",true);
            json.put("message","Device Have Added");
        }else{
            json.put("status",false);
            json.put("message","Server Error");
        }
        return json;
    }
}

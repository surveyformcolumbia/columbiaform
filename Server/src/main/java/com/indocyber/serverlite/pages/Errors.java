package com.indocyber.serverlite.pages;

import com.indocyber.serverlite.lib.Logger;
import org.json.JSONObject;
import spark.Request;
import spark.Response;

/**
 * Created by Indocyber on 08/07/2017.
 */
public class Errors extends Page {
    public static String error500(Request request , Response response){
        new Logger().printLog(request);
        response.type("application/json");
        JSONObject json=new JSONObject();
        json.put("status",false);
        json.put("message","500 Internal Server Error");
        return json.toString();
    }
    public static String error404(Request request , Response response){
        new Logger().printLog(request);
        response.type("application/json");
        JSONObject json=new JSONObject();
        json.put("status",false);
        json.put("message","404 No End Point Found");
        return json.toString();
    }
}

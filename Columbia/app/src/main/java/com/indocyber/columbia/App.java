package com.indocyber.columbia;

import android.app.Application;
import android.content.Context;
import android.support.multidex.MultiDex;

/**
 * Created by Indocyber on 19/07/2017.
 */

public class App extends Application {
    /*
    init multi dex when attach context
     */
    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);
        MultiDex.install(this);
    }

}

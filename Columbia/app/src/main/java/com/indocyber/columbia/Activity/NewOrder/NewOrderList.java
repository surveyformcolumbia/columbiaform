package com.indocyber.columbia.Activity.NewOrder;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.LinearLayout;

import com.indocyber.columbia.Adapter.ListNewOrderAdapter;
import com.indocyber.columbia.Model.ItemNewOrder;
import com.indocyber.columbia.R;

import java.util.ArrayList;
import java.util.List;

public class NewOrderList extends AppCompatActivity implements ListNewOrderAdapter.OnListItemClick {

    private ListNewOrderAdapter listNewOrderAdapter;
    private List<ItemNewOrder> list = new ArrayList<>();
    private RecyclerView recyclerView;
    private ItemNewOrder model;
    private int posision = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_order_list);

        listNewOrderAdapter = new ListNewOrderAdapter(list);
        listNewOrderAdapter.setOnListItemClick(this);

        //Init RecycleView
        recyclerView = (RecyclerView) findViewById(R.id.newOrder_recycler_view);
        RecyclerView.LayoutManager layoutManager = new GridLayoutManager(getApplicationContext(), 1);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setHasFixedSize(true);
        recyclerView.setAdapter(listNewOrderAdapter);

    }

    @Override
    public void onItemClick(View v, int position, ItemNewOrder model) {
        this.posision = position;
        this.model = model;
        LinearLayout layout = (LinearLayout) v.findViewById(R.id.itemNewOrder_container);
        layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                callInput();
            }
        });

    }

    private void callInput (){
        Intent intent = new Intent(NewOrderList.this,NewOrderActivity.class);
        startActivity(intent);
    }
}

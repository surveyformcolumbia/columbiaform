package com.indocyber.columbia.Activity.NewOrder;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.indocyber.columbia.Abstract.ChildAppActivity;
import com.indocyber.columbia.Adapter.ListNewOrderAdapter;
import com.indocyber.columbia.Contract.NewOrderInterface;
import com.indocyber.columbia.Model.ItemNewOrder;
import com.indocyber.columbia.Model.OrderEntry;
import com.indocyber.columbia.R;

import java.util.ArrayList;
import java.util.List;

public class NewOrderActivity extends ChildAppActivity implements
        NewOrderInterface{

    private SectionsPagerAdapter mSectionsPagerAdapter;
    private ViewPager mViewPager;
    public OrderEntry orderEntry;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_order);

        //Set ViewPager
        mSectionsPagerAdapter = new SectionsPagerAdapter(getSupportFragmentManager());
        mViewPager = (ViewPager) findViewById(R.id.container);
        mViewPager.setAdapter(mSectionsPagerAdapter);
        mViewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                try {
                    switch (position) {
                        case 0:
                            getSupportActionBar().setTitle("Data Pribadi I (1/24)");
                            break;
                        case 1:
                            getSupportActionBar().setTitle("Data Pribadi II (2/24)");
                            break;
                        case 2:
                            getSupportActionBar().setTitle("Data Pribadi III (3/24)");
                            break;
                        case 3:
                            getSupportActionBar().setTitle("Data Pribadi IV (4/24)");
                            break;
                        case 4:
                            getSupportActionBar().setTitle("Data Pribadi V (5/24)");
                            break;
                        case 5:
                            getSupportActionBar().setTitle("Data Pribadi VI (6/24)");
                            break;
                        case 6:
                            getSupportActionBar().setTitle("Data Pasangan I (7/24)");
                            break;
                        case 7:
                            getSupportActionBar().setTitle("Data Keuangan I (8/24)");
                            break;
                        case 8:
                            getSupportActionBar().setTitle("Kerabat tidak Serumah (9/24)");
                            break;
                        case 9:
                            getSupportActionBar().setTitle("Data Anak(10/24)");
                            break;
                        case 10:
                            getSupportActionBar().setTitle("General (11/24)");
                            break;
                        case 11:
                            getSupportActionBar().setTitle("Dealer (12/24)");
                            break;
                        case 12:
                            getSupportActionBar().setTitle("Data Barang & Pinjaman (13/24)");
                            break;
                        case 13:
                            getSupportActionBar().setTitle("Penghasilan Suami/Istri Konsumen (14/24)");
                            break;
                        case 14:
                            getSupportActionBar().setTitle("Penghasilan Tidak tetap (15/24)");
                            break;
                        case 15:
                            getSupportActionBar().setTitle("Pemilikan Aset Lain (16/24)");
                            break;
                        case 16:
                            getSupportActionBar().setTitle("Keuangan dan Perbankan I (17/24)");
                            break;
                        case 17:
                            getSupportActionBar().setTitle("Keuangan dan Perbankan II (18/24)");
                            break;
                        case 18:
                            getSupportActionBar().setTitle("Summary Penghasilan / Pengeluaran (19/24)");
                            break;
                        case 19:
                            getSupportActionBar().setTitle("Kekayaan / Harta Konsumen (20/24)");
                            break;
                        case 20:
                            getSupportActionBar().setTitle("Kepribadiaan Konsumen(21/24)");
                            break;
                        case 21:
                            getSupportActionBar().setTitle("Lain - Lain(22/24)");
                            break;
                        case 22:
                            getSupportActionBar().setTitle("Kondisi Konsumen (23/24)");
                            break;
                        case 23:
                            getSupportActionBar().setTitle("Memo(24/24)");
                            break;

                    }
                }catch (NullPointerException e){
                    e.printStackTrace();
                }

            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });

        orderEntry = new OrderEntry();
    }

    @Override
    public OrderEntry getOrderEntry() {
        return orderEntry;
    }

    public class SectionsPagerAdapter extends FragmentPagerAdapter {

        public SectionsPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            switch (position) {
                case 0:
                    return NewOrder1.newInstance();
                case 1:
                    return NewOrder2.newInstance();
                case 2:
                    return NewOrder3.newInstance();
                case 3:
                    return NewOrder4.newInstance();
                case 4:
                    return NewOrder5.newInstance();
                case 5:
                    return NewOrder6.newInstance();
                case 6:
                    return NewOrder7.newInstance();
                case 7:
                    return NewOrder8.newInstance();
                case 8:
                    return NewOrder9.newInstance();
                case 9:
                    return NewOrder10.newInstance();
                case 10:
                    return NewOrder11.newInstance();
                case 11:
                    return NewOrder12.newInstance();
                case 12:
                    return NewOrder13.newInstance();
                case 13:
                    return NewOrder14.newInstance();
                case 14:
                    return NewOrder15.newInstance();
                case 15:
                    return NewOrder16.newInstance();
                case 16:
                    return NewOrder17.newInstance();
                case 17:
                    return NewOrder18.newInstance();
                case 18:
                    return NewOrder19.newInstance();
                case 19:
                    return NewOrder20.newInstance();
                case 20:
                    return NewOrder21.newInstance();
                case 21:
                    return NewOrder22.newInstance();
                case 22:
                    return NewOrder23.newInstance();
                case 23:
                    return NewOrder24.newInstance();

            }
            return null;
        }

        @Override
        public int getCount() {
            return 24;
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return null;
        }
    }
}

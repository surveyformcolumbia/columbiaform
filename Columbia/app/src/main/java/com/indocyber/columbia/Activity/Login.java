package com.indocyber.columbia.Activity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import com.indocyber.columbia.Abstract.AppActivity;
import com.indocyber.columbia.R;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import org.json.JSONException;
import org.json.JSONObject;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import cz.msebera.android.httpclient.Header;

public class Login extends AppActivity implements View.OnClickListener {
    private Button btnLogin,btnSetting;
    private EditText txtUsername,txtPassword;
    private TextView txtError;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        /*
        init component
         */
        txtError= (TextView) findViewById(R.id.textViewError);
        btnLogin= (Button) findViewById(R.id.buttonLogin);
        btnSetting= (Button) findViewById(R.id.buttonLoginConfiguration);
        txtUsername= (EditText) findViewById(R.id.editTextLoginUserID);
        txtPassword= (EditText) findViewById(R.id.editTextLoginPassword);
        //set onclick listener to this class
        btnLogin.setOnClickListener(this);
        btnSetting.setOnClickListener(this);
    }
    /*
    for all click event below
     */
    @Override
    public void onClick(View v) {
        /*
        choose event by element
         */
        switch (v.getId()){
            case R.id.buttonLogin:
                /*
                store text temp
                 */
                String username=txtUsername.getText().toString();
                String password=txtPassword.getText().toString();
                /*
                validate input
                 */
                if (username.equals("")){
                    txtError.setText("Username is required");
                    txtError.setVisibility(View.VISIBLE);
                    return;
                }
                if (password.equals("")){
                    txtError.setText("Password is required");
                    txtError.setVisibility(View.VISIBLE);
                    return;
                }
                /*
                encrypt password
                 */
                StringBuffer sb = new StringBuffer();
                try {
                    MessageDigest md5 = MessageDigest.getInstance("MD5");
                    md5.update(password.getBytes());
                    byte[] digest = md5.digest();
                    for (byte b : digest) {
                        sb.append(String.format("%02x", b & 0xff));
                    }
                    password=sb.toString();
                    /*
                    set loading
                     */
                    loading=new ProgressDialog(this);
                    loading.setProgressStyle(ProgressDialog.STYLE_SPINNER);
                    loading.setCancelable(false);
                    loading.setMessage("Sending login information ...");
                    loading.show();
                    /*
                    store paramerters
                     */
                    RequestParams params=new RequestParams();
                    params.put("device_id",session.getDevice());
                    params.put("password",password);
                    params.put("username",username);
                    httpClient.post(session.getUrl()+config.LOGIN_ENDPOINT,params,new JsonHttpResponseHandler(){
                        /*
                        handle success login
                         */
                        public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                            loading.hide();
                            try {
                                /*
                                if login succes and valid login
                                 */
                                if(response.getBoolean("status")){
                                    /*
                                    store token and userdata
                                     */
                                    session.setLogin(true);
                                    session.setToken(response.getString("token"));
                                    session.setUserData(response.getJSONArray("data").getJSONObject(0));
                                    finish();
                                }else{
                                    /*
                                    show reason error
                                     */
                                    txtError.setText(response.getString("message"));
                                    txtError.setVisibility(View.VISIBLE);
                                }
                            } catch (JSONException e) {
                                Log.e(getClass().getSimpleName(),e.getMessage());
                                txtError.setText("Client Error");
                                txtError.setVisibility(View.VISIBLE);
                            }
                        }
                        /*
                        handle error login
                         */
                        public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                            loading.hide();
                            /*
                            show error server
                             */
                            Log.e(getClass().getSimpleName(),responseString);
                            txtError.setText("Server Error");
                            txtError.setVisibility(View.VISIBLE);
                        }
                    });
                } catch (NoSuchAlgorithmException e) {
                    Log.e(getClass().getSimpleName(),e.getMessage());
                }
                break;
            case  R.id.buttonLoginConfiguration :
                Intent config=new Intent(this,Configuration.class);
                startActivity(config);
                break;
        }
    }
}

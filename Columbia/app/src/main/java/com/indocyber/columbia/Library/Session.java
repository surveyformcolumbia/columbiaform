package com.indocyber.columbia.Library;

import android.content.Context;
import android.content.SharedPreferences;
import com.indocyber.columbia.Config.Global;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by Indocyber on 07/07/2017.
 * library untuk menyimpan semua session dan pengaturan system
 */
public class Session {
    private Context ctx;
    private SharedPreferences preferences;
    private SharedPreferences.Editor editor;
    private final String IS_LOGIN="is_login";
    private final String USERDATA="user";
    private final String HASHCODE="hash";
    private final String IMEI="imei";
    private final String CONFIGURATION="config";
    private final String TOKEN="token";
    private final String IS_CONFIGURED="is_configured";
    private final String IS_SYNCED="is_synced";
    private final String URL="http://";
    private Global config;
    public Session(Context ctx){
        this.config=new Global();
        this.ctx=ctx;
        this.preferences=this.ctx.getSharedPreferences(this.config.PREF_NAME,Context.MODE_PRIVATE);
        this.editor=this.preferences.edit();
    }
    public void setLogin(Boolean login){
        editor.putBoolean(IS_LOGIN,login);
        editor.commit();
    }
    public void setSynced(Boolean synced){
        editor.putBoolean(IS_SYNCED,synced);
        editor.commit();
    }
    public void setConfigured(Boolean configured){
        editor.putBoolean(IS_CONFIGURED,configured);
        editor.commit();
    }
    public void setURL(String url){
        editor.putString(URL,url);
        editor.commit();
    }
    public void setToken(String token){
        editor.putString(TOKEN,token);
        editor.commit();
    }
    public void setDevice(String device){
        editor.putString(IMEI,device);
        editor.commit();
    }
    public void setHash(String hash){
        editor.putString(HASHCODE,hash);
        editor.commit();
    }
    public void setUserData(JSONObject userData){
        editor.putString(USERDATA,userData.toString());
        editor.commit();
    }
    public void setConfiguration(JSONObject configuration){
        editor.putString(CONFIGURATION,configuration.toString());
        editor.commit();
    }
    public Boolean isLogin(){
        return this.preferences.getBoolean(IS_LOGIN,false);
    }
    public Boolean isSynced(){
        return this.preferences.getBoolean(IS_SYNCED,false);
    }
    public Boolean isConfigured(){
        return this.preferences.getBoolean(IS_CONFIGURED,false);
    }
    public JSONObject getUserdata() throws JSONException {
        String json=this.preferences.getString(USERDATA,"{}");
        return new JSONObject(json);
    }
    public JSONObject getConfig() throws JSONException {
        String json=this.preferences.getString(CONFIGURATION,"{}");
        return new JSONObject(json);
    }
    public String getUrl(){
        String url=this.preferences.getString(URL,"");
        return url;
    }
    public String getHash(){
        String hash=this.preferences.getString(HASHCODE,"");
        return hash;
    }
    public String getDevice(){
        String imei=this.preferences.getString(IMEI,"");
        return imei;
    }
    public String getToken(){
        String token=this.preferences.getString(TOKEN,"");
        return token;
    }
}

package com.indocyber.columbia.Contract;

import com.indocyber.columbia.Model.OrderEntry;

/**
 * Created by Frendi on 20/07/2017.
 */

public interface NewOrderInterface {

    OrderEntry getOrderEntry();

}

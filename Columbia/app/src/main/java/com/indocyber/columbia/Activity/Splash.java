package com.indocyber.columbia.Activity;

import android.Manifest;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.os.Build;
import android.os.Handler;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.RequiresApi;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.ActivityCompat.OnRequestPermissionsResultCallback;
import android.telephony.TelephonyManager;
import android.util.Base64;
import android.util.Log;
import android.widget.TextView;
import android.widget.Toast;
import com.indocyber.columbia.Abstract.AppActivity;
import com.indocyber.columbia.R;
import com.indocyber.columbia.Service.SyncDatabase;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

public class Splash extends AppActivity implements OnRequestPermissionsResultCallback {
    private TextView textLoading;
    private int currentStep=1;
    private boolean permissionRequest=true;
    public boolean syncStatus=false;
    public String syncMessage="";
    private SyncDBReceiver receiver;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        textLoading= (TextView) findViewById(R.id.textViewLoading);
        try {
            /*
            set hash for App
             */
            PackageInfo info = getPackageManager().getPackageInfo(getPackageName(), PackageManager.GET_SIGNATURES);
            for (Signature signature : info.signatures) {
                MessageDigest md = MessageDigest.getInstance("SHA");
                md.update(signature.toByteArray());
                session.setHash(Base64.encodeToString(md.digest(), Base64.DEFAULT));
            }
            setStep(1);
        } catch (PackageManager.NameNotFoundException e) {
            Log.e(getClass().getSimpleName(),e.getMessage());
        } catch (NoSuchAlgorithmException e) {
            Log.e(getClass().getSimpleName(),e.getMessage());

        }
        IntentFilter filter = new IntentFilter(SyncDBReceiver.PROCESS_RESPONSE);
        filter.addCategory(Intent.CATEGORY_DEFAULT);
        receiver = new SyncDBReceiver();
        registerReceiver(receiver, filter);
    }
    @Override
    protected void onResume() {
        super.onResume();
       setStep(1);
    }
    @Override
    public void onDestroy() {
        super.onDestroy();
        unregisterReceiver(receiver);
    }

    /*
    step splash
     */
    private void setStep(final int step){
        currentStep=step;
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                switch (step){
                    case 1 :
                        textLoading.setText("Check Permission ...");
                        permitStep();
                        break;
                    case 2 :
                        textLoading.setText("Check App Configuration ...");
                        configStep();
                        break;
                    case 3 :
                        textLoading.setText("Load User Information ...");
                        loginStep();
                        break;
                    case 4 :
                        textLoading.setText("Get Data From Server ...");
                        syncStep();
                        break;
                }
            }
        }, config.SPLASH);
    }
    /*
    step permission 1
     */
    private void permitStep(){
        if(checkPermission()){
            setStep(2);
        }else{
            /*
            marshmallow above
             */
            if(Build.VERSION.SDK_INT>=22){
                requestPermissions();
            }else{
                permissionRequest=false;
                setStep(2);
            }
        }
    }
    /*
    step configuration 2
     */
    private void configStep(){
        if (session.isConfigured()){
            /*
            get device ID
             */
            TelephonyManager mngr = (TelephonyManager)getSystemService(Context.TELEPHONY_SERVICE);
            session.setDevice(mngr.getDeviceId());
            setStep(3);
        }else{
            Intent con=new Intent(this,Configuration.class);
            startActivity(con);
        }
    }
    /*
    step login checking 3
     */
    private void loginStep(){
        if(session.isLogin()){
            setStep(4);
        }else{
            Intent login = new Intent(this,Login.class);
            startActivity(login);
        }
    }
    /*
    step sync database 4
     */
    private void syncStep(){
        //if already synced
        if(session.isSynced()){
            Intent main = new Intent(this,Main.class);
            startActivity(main);
        }else {
            Intent sync = new Intent(this, SyncDatabase.class);
            startService(sync);
        }
    }
    /*
    step sync 5 check sync
     */
    public void checkSync(){
        /*
        if sync success
         */
        if(syncStatus){
            session.setSynced(true);
            Intent main = new Intent(this,Main.class);
            startActivity(main);
        }else{
            settingDialog(syncMessage);
        }
    }
    /*
    check permission in realtime
     */
    private Boolean checkPermission(){
        int phoneState= ActivityCompat.checkSelfPermission(this, Manifest.permission.READ_PHONE_STATE);
        int storagePermission= ActivityCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE);
        int storagePermissionW= ActivityCompat.checkSelfPermission(this,Manifest.permission.WRITE_EXTERNAL_STORAGE);
        int cameraP= ActivityCompat.checkSelfPermission(this,Manifest.permission.CAMERA);
        int locationCoarse=ActivityCompat.checkSelfPermission(this,Manifest.permission.ACCESS_COARSE_LOCATION);
        int locationFine=ActivityCompat.checkSelfPermission(this,Manifest.permission.ACCESS_FINE_LOCATION);
        if(phoneState==PackageManager.PERMISSION_GRANTED && storagePermission==PackageManager.PERMISSION_GRANTED && locationCoarse==PackageManager.PERMISSION_GRANTED
                && locationFine==PackageManager.PERMISSION_GRANTED && storagePermissionW==PackageManager.PERMISSION_GRANTED
                &&cameraP==PackageManager.PERMISSION_GRANTED){
            return true;
        }else{
            return false;
        }
    }
    //Requesting permission
    private void requestPermissions() {
        if (permissionRequest) {
            permissionRequest=false;
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.READ_EXTERNAL_STORAGE,
                    Manifest.permission.CAMERA, Manifest.permission.ACCESS_COARSE_LOCATION,
                    Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.WRITE_EXTERNAL_STORAGE,
                    Manifest.permission.READ_PHONE_STATE}, config.CODE_PERMISSION);
        }
    }
    /*
    on get permission from intent
     */
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults){
        if(requestCode==config.CODE_PERMISSION){
            if(grantResults.length>0 && grantResults[0] == PackageManager.PERMISSION_GRANTED && grantResults[1] == PackageManager.PERMISSION_GRANTED
                    && grantResults[2] == PackageManager.PERMISSION_GRANTED
                    && grantResults[3] == PackageManager.PERMISSION_GRANTED && grantResults[4] == PackageManager.PERMISSION_GRANTED){
                setStep(2);
            }else{
                Toast.makeText(this,"Please Grant Our Permission !",Toast.LENGTH_SHORT).show();
                finish();
            }
        }
    }

    public class SyncDBReceiver extends BroadcastReceiver {
        public static final String PROCESS_RESPONSE = "com.indocyber.columbia.intent.action.PROCESS_RESPONSE";
        @Override
        public void onReceive(Context context, Intent intent) {
            String TYPE=intent.getStringExtra("type");
            if(TYPE.equals("RESULT")) {
                syncStatus = intent.getBooleanExtra("status", false);
                syncMessage = intent.getStringExtra("message");
                checkSync();
            }else{
                textLoading.setText("Cloned Object "+intent.getStringExtra("size")+" ..");
            }
        }
    }
}


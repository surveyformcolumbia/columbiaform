package com.indocyber.columbia.Activity.NewOrder;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.indocyber.columbia.Contract.NewOrderInterface;
import com.indocyber.columbia.Model.OrderEntry;
import com.indocyber.columbia.R;
public class NewOrder1 extends Fragment {

    private static final String TAG = "NewOrderPart1";
    private NewOrderInterface newOrderInterface;
    OrderEntry orderEntry = null;

    public NewOrder1() {
    }

    public static NewOrder1 newInstance (){
        return new NewOrder1();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.d(TAG, "onCreate: ");
        orderEntry = newOrderInterface.getOrderEntry();
        ((AppCompatActivity)getActivity()).getSupportActionBar().setTitle("Data Pribadi I (1/24)");
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_new_order1, container, false);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        try {
            newOrderInterface = (NewOrderInterface) context;
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    private void setupView (){
        Log.d(TAG, "setupView: ");
    }

}

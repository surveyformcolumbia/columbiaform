package com.indocyber.columbia.Activity;

import android.os.Bundle;
import android.util.Log;

import com.indocyber.columbia.Abstract.ChildAppActivity;
import com.indocyber.columbia.Model.OrderEntry;
import com.indocyber.columbia.R;
import io.realm.RealmQuery;
import io.realm.RealmResults;

public class SavedOrder extends ChildAppActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_saved_order);
        RealmQuery<OrderEntry> query = this.realm.where(OrderEntry.class);
        RealmResults<OrderEntry> result = query.findAll();
        Log.i("Result",result.toString());
    }
}

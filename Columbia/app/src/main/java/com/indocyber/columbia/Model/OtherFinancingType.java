package com.indocyber.columbia.Model;

import io.realm.RealmObject;

/**
 * Created by Indocyber on 17/07/2017.
 */

public class OtherFinancingType extends RealmObject {
    private String FINANCING_TYPE_ID;
    private String FINANCING_TYPE_NAME;
    private String ACTIVE_STATUS;
    private String CREATED_BY;
    private String CREATED_DATE;
    private String MODIFIED_BY;
    private String MODIFIED_DATE;
    public String getFINANCING_TYPE_ID() {
        return FINANCING_TYPE_ID;
    }

    public void setFINANCING_TYPE_ID(String FINANCING_TYPE_ID) {
        this.FINANCING_TYPE_ID = FINANCING_TYPE_ID;
    }

    public String getFINANCING_TYPE_NAME() {
        return FINANCING_TYPE_NAME;
    }

    public void setFINANCING_TYPE_NAME(String FINANCING_TYPE_NAME) {
        this.FINANCING_TYPE_NAME = FINANCING_TYPE_NAME;
    }

    public String getACTIVE_STATUS() {
        return ACTIVE_STATUS;
    }

    public void setACTIVE_STATUS(String ACTIVE_STATUS) {
        this.ACTIVE_STATUS = ACTIVE_STATUS;
    }

    public String getCREATED_BY() {
        return CREATED_BY;
    }

    public void setCREATED_BY(String CREATED_BY) {
        this.CREATED_BY = CREATED_BY;
    }

    public String getCREATED_DATE() {
        return CREATED_DATE;
    }

    public void setCREATED_DATE(String CREATED_DATE) {
        this.CREATED_DATE = CREATED_DATE;
    }

    public String getMODIFIED_BY() {
        return MODIFIED_BY;
    }

    public void setMODIFIED_BY(String MODIFIED_BY) {
        this.MODIFIED_BY = MODIFIED_BY;
    }

    public String getMODIFIED_DATE() {
        return MODIFIED_DATE;
    }

    public void setMODIFIED_DATE(String MODIFIED_DATE) {
        this.MODIFIED_DATE = MODIFIED_DATE;
    }
}

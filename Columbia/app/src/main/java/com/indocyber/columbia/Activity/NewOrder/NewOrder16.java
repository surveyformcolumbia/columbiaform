package com.indocyber.columbia.Activity.NewOrder;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.indocyber.columbia.Contract.NewOrderInterface;
import com.indocyber.columbia.Model.OrderEntry;
import com.indocyber.columbia.R;

public class NewOrder16 extends Fragment {

    private static final String TAG = "NewOrder16";
    private NewOrderInterface newOrderInterface;
    private OrderEntry orderEntry = null;

    public static NewOrder16 newInstance() {
        return new NewOrder16();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        orderEntry = newOrderInterface.getOrderEntry();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_new_order16, container, false);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        try {
            newOrderInterface = (NewOrderInterface) context;
        }catch (Exception e){
            e.printStackTrace();
        }
    }


}

package com.indocyber.columbia.Service;

import android.app.IntentService;
import android.content.Intent;
import android.support.annotation.Nullable;
import android.util.Log;

import com.indocyber.columbia.Config.Global;
import com.indocyber.columbia.Library.Session;
import com.indocyber.columbia.Model.Combo;
import com.indocyber.columbia.Model.JobTitle;
import com.indocyber.columbia.Model.Occupation;
import com.indocyber.columbia.Model.OrderEntry;
import com.indocyber.columbia.Model.OtherFinancingType;
import com.indocyber.columbia.Model.ProductClasification;
import com.indocyber.columbia.Model.ProductCondition;
import com.indocyber.columbia.Model.Province;
import com.indocyber.columbia.Model.Relation;
import com.indocyber.columbia.Model.Religion;
import com.indocyber.columbia.Model.StatusKunjungan;
import com.indocyber.columbia.Model.Zip;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import com.loopj.android.http.SyncHttpClient;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import cz.msebera.android.httpclient.Header;
import cz.msebera.android.httpclient.entity.mime.content.StringBody;
import io.realm.Realm;
import io.realm.RealmConfiguration;
import io.realm.internal.IOException;

/**
 * Created by Indocyber on 14/07/2017.
 */

public class SyncDatabase extends IntentService {
    private SyncHttpClient httpClient;
    private boolean isGetedList=false;
    private Realm realm;
    private Global config;
    private Session session;
    private JSONArray listObject;
    private long sizeObject;
    private int currentIndex=0;
    public SyncDatabase() {
        super("Sync Database");
    }

    @Override
    protected void onHandleIntent(@Nullable final Intent intent) {
        /*
        create all library on create service
         */
        Realm.init(getApplicationContext());
        RealmConfiguration configRealm = new RealmConfiguration.Builder()
                .deleteRealmIfMigrationNeeded()
                .build();
        this.realm=Realm.getInstance(configRealm);
        this.httpClient=new SyncHttpClient();
        this.config=new Global();
        this.session=new Session(this);
        if(!isGetedList) {
            RequestParams params = new RequestParams();
            params.put("token", session.getToken());
            httpClient.get(this, session.getUrl() + config.SYNC_ENDPOINT, params, new JsonHttpResponseHandler() {
                /*
                on get all object list
                 */
                public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                    try {
                        if (response.getBoolean("status")) {
                            isGetedList=true;
                            listObject = response.getJSONArray("data");
                            if (listObject.length() > 0) {
                                getObject(currentIndex);
                            } else {
                                setSuccess();
                            }
                        } else {
                            Log.e(getClass().getSimpleName(), response.getString("message"));
                            setFailed();
                        }
                    } catch (JSONException e) {
                        Log.e(getClass().getSimpleName(), e.getMessage());
                        setFailed();
                    }
                }

                @Override
                public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                    Log.e(getClass().getSimpleName(), throwable.getMessage());
                    setFailed();
                }

                @Override
                public void onFailure(int status, Header[] headers, Throwable throwable, JSONObject response) {
                    Log.e(getClass().getSimpleName(), throwable.getMessage());
                    setFailed();
                }
            });
        }
    }
    private void getObject(final int index){
        try {
            final String objectName=listObject.getString(currentIndex);
            RequestParams params=new RequestParams();
            params.put("token",session.getToken());
            params.put("object",objectName);
            httpClient.get(this,session.getUrl()+config.OBJECT_ENDPOINT,params,new JsonHttpResponseHandler(){
                /*
                on get all object list
                 */
                public void onSuccess(int statusCode, Header[] headers, final JSONObject response) {
                    try {
                        if(response.getBoolean("status")){
                            setProgress(response.getJSONArray("data").length());
                            /*
                            insert Data
                             */
                            realm.executeTransaction(new Realm.Transaction() {
                                @Override
                                public void execute(Realm realm) {
                                    try {
                                        /*
                                        switch action to do
                                         */
                                        switch (objectName){
                                            case  "MOBILEORDER" :
                                                realm.delete(OrderEntry.class);
                                                realm.createAllFromJson(OrderEntry.class, response.getJSONArray("data"));
                                                break;
                                            case  "CUSTOMER_JOB_TITLE_MST" :
                                                realm.delete(JobTitle.class);
                                                realm.createAllFromJson(JobTitle.class, response.getJSONArray("data"));
                                                break;
                                            case  "OCCUPATION_MST" :
                                                realm.delete(Occupation.class);
                                                realm.createAllFromJson(Occupation.class, response.getJSONArray("data"));
                                                break;
                                            case  "OTHER_FINANCING_TYPE_MST" :
                                                realm.delete(OtherFinancingType.class);
                                                realm.createAllFromJson(OtherFinancingType.class, response.getJSONArray("data"));
                                                break;
                                            case  "PRODUCT_CLASSIFICATION_MST" :
                                                realm.delete(ProductClasification.class);
                                                realm.createAllFromJson(ProductClasification.class, response.getJSONArray("data"));
                                                break;
                                            case  "PRODUCT_CONDITION_MST" :
                                                realm.delete(ProductCondition.class);
                                                realm.createAllFromJson(ProductCondition.class, response.getJSONArray("data"));
                                                break;
                                            case  "PROVINCE_MST" :
                                                realm.delete(Province.class);
                                                realm.createAllFromJson(Province.class, response.getJSONArray("data"));
                                                break;
                                            case  "REFERENCE_RELATION_MST" :
                                                realm.delete(Relation.class);
                                                realm.createAllFromJson(Relation.class, response.getJSONArray("data"));
                                                break;
                                            case  "RELIGION_MST" :
                                                realm.delete(Religion.class);
                                                realm.createAllFromJson(Religion.class, response.getJSONArray("data"));
                                                break;
                                            case  "STATUS_KUNJUNGAN_MST" :
                                                realm.delete(StatusKunjungan.class);
                                                realm.createAllFromJson(StatusKunjungan.class, response.getJSONArray("data"));
                                                break;
                                            case  "COMBOBOX" :
                                                realm.delete(Combo.class);
                                                realm.createAllFromJson(Combo.class, response.getJSONArray("data"));
                                                break;
                                            case  "ZIP" :
                                                realm.delete(Zip.class);
                                                realm.createAllFromJson(Zip.class, response.getJSONArray("data"));
                                                break;
                                        }
                                    } catch (IOException | JSONException e) {
                                        Log.e(getClass().getSimpleName(),e.getMessage());
                                        setFailed();
                                    }
                                }
                            });
                            /*
                            if still a data get new data object
                             */
                            if(currentIndex<listObject.length()-1){
                                currentIndex++;
                                getObject(currentIndex);
                            }else{
                                setSuccess();
                            }
                        }else{
                            getObject(index);
                            Log.e(getClass().getSimpleName(),response.getString("message"));
                            setFailed();
                        }
                    } catch (JSONException e) {
                        Log.e(getClass().getSimpleName(),e.getMessage());
                        setFailed();
                    }
                }
                @Override
                public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                    Log.e(getClass().getSimpleName(),throwable.getMessage());
                    setFailed();
                }
                @Override
                public void onFailure(int status, Header[] headers, Throwable throwable, JSONObject response){
                    Log.e(getClass().getSimpleName(),throwable.getMessage());
                    setFailed();
                }
            });
        } catch (JSONException e) {
            setFailed();
            Log.e(getClass().getSimpleName(),e.getMessage());
        }
    }
    private void setSuccess(){
        Intent broadcastIntent = new Intent();
        broadcastIntent.setAction("com.indocyber.columbia.intent.action.PROCESS_RESPONSE");
        broadcastIntent.addCategory(Intent.CATEGORY_DEFAULT);
        broadcastIntent.putExtra("type", "RESULT");
        broadcastIntent.putExtra("status", true);
        broadcastIntent.putExtra("message", "sync success");
        sendBroadcast(broadcastIntent);
    }
    private void setFailed(){
        Intent broadcastIntent = new Intent();
        broadcastIntent.setAction("com.indocyber.columbia.intent.action.PROCESS_RESPONSE");
        broadcastIntent.addCategory(Intent.CATEGORY_DEFAULT);
        broadcastIntent.putExtra("type", "RESULT");
        broadcastIntent.putExtra("status", false);
        broadcastIntent.putExtra("message", "Can't get data from server. Check your internet connection or server address !");
        sendBroadcast(broadcastIntent);
    }
    private void setProgress(int size){
        sizeObject+=size;
        Intent broadcastIntent = new Intent();
        broadcastIntent.setAction("com.indocyber.columbia.intent.action.PROCESS_RESPONSE");
        broadcastIntent.addCategory(Intent.CATEGORY_DEFAULT);
        broadcastIntent.putExtra("type", "REALM");
        broadcastIntent.putExtra("size", String.valueOf(sizeObject));
        sendBroadcast(broadcastIntent);
    }
}

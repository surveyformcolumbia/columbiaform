package com.indocyber.columbia.Model;

import io.realm.RealmObject;

/**
 * Created by Indocyber on 17/07/2017.
 */

public class OrderEntry extends RealmObject {
    public String getDATA_MAP_NAME() {
        return DATA_MAP_NAME;
    }

    public void setDATA_MAP_NAME(String DATA_MAP_NAME) {
        this.DATA_MAP_NAME = DATA_MAP_NAME;
    }

    public String getORDERID() {
        return ORDERID;
    }

    public void setORDERID(String ORDERID) {
        this.ORDERID = ORDERID;
    }

    public String getMODULE_ID() {
        return MODULE_ID;
    }

    public void setMODULE_ID(String MODULE_ID) {
        this.MODULE_ID = MODULE_ID;
    }

    public String getITEM_TYPE() {
        return ITEM_TYPE;
    }

    public void setITEM_TYPE(String ITEM_TYPE) {
        this.ITEM_TYPE = ITEM_TYPE;
    }

    public String getITEM_KEY() {
        return ITEM_KEY;
    }

    public void setITEM_KEY(String ITEM_KEY) {
        this.ITEM_KEY = ITEM_KEY;
    }

    public String getUSER_ID() {
        return USER_ID;
    }

    public void setUSER_ID(String USER_ID) {
        this.USER_ID = USER_ID;
    }

    public String getSTATUS() {
        return STATUS;
    }

    public void setSTATUS(String STATUS) {
        this.STATUS = STATUS;
    }

    public String getSTATUS_UPDATE() {
        return STATUS_UPDATE;
    }

    public void setSTATUS_UPDATE(String STATUS_UPDATE) {
        this.STATUS_UPDATE = STATUS_UPDATE;
    }

    public String getTIMESTAMPS() {
        return TIMESTAMPS;
    }

    public void setTIMESTAMPS(String TIMESTAMPS) {
        this.TIMESTAMPS = TIMESTAMPS;
    }

    public String getLAST_TRX() {
        return LAST_TRX;
    }

    public void setLAST_TRX(String LAST_TRX) {
        this.LAST_TRX = LAST_TRX;
    }

    public String getBRANCH_ID() {
        return BRANCH_ID;
    }

    public void setBRANCH_ID(String BRANCH_ID) {
        this.BRANCH_ID = BRANCH_ID;
    }

    public String getSTATUS_KUNJUNGAN() {
        return STATUS_KUNJUNGAN;
    }

    public void setSTATUS_KUNJUNGAN(String STATUS_KUNJUNGAN) {
        this.STATUS_KUNJUNGAN = STATUS_KUNJUNGAN;
    }

    public String getBLANK() {
        return BLANK;
    }

    public void setBLANK(String BLANK) {
        this.BLANK = BLANK;
    }

    public String getNAME() {
        return NAME;
    }

    public void setNAME(String NAME) {
        this.NAME = NAME;
    }

    public String getPERS_IDENTIFY_TYPE() {
        return PERS_IDENTIFY_TYPE;
    }

    public void setPERS_IDENTIFY_TYPE(String PERS_IDENTIFY_TYPE) {
        this.PERS_IDENTIFY_TYPE = PERS_IDENTIFY_TYPE;
    }

    public String getIDENTIFY_NO() {
        return IDENTIFY_NO;
    }

    public void setIDENTIFY_NO(String IDENTIFY_NO) {
        this.IDENTIFY_NO = IDENTIFY_NO;
    }

    public String getNPWP_NO() {
        return NPWP_NO;
    }

    public void setNPWP_NO(String NPWP_NO) {
        this.NPWP_NO = NPWP_NO;
    }

    public String getCUST_BIRTH_DATE() {
        return CUST_BIRTH_DATE;
    }

    public void setCUST_BIRTH_DATE(String CUST_BIRTH_DATE) {
        this.CUST_BIRTH_DATE = CUST_BIRTH_DATE;
    }

    public String getPERS_RELIGION() {
        return PERS_RELIGION;
    }

    public void setPERS_RELIGION(String PERS_RELIGION) {
        this.PERS_RELIGION = PERS_RELIGION;
    }

    public String getADDRESS_KTP() {
        return ADDRESS_KTP;
    }

    public void setADDRESS_KTP(String ADDRESS_KTP) {
        this.ADDRESS_KTP = ADDRESS_KTP;
    }

    public String getRT() {
        return RT;
    }

    public void setRT(String RT) {
        this.RT = RT;
    }

    public String getRW() {
        return RW;
    }

    public void setRW(String RW) {
        this.RW = RW;
    }

    public String getKELURAHAN() {
        return KELURAHAN;
    }

    public void setKELURAHAN(String KELURAHAN) {
        this.KELURAHAN = KELURAHAN;
    }

    public String getKECAMATAN() {
        return KECAMATAN;
    }

    public void setKECAMATAN(String KECAMATAN) {
        this.KECAMATAN = KECAMATAN;
    }

    public String getKABKODYA() {
        return KABKODYA;
    }

    public void setKABKODYA(String KABKODYA) {
        this.KABKODYA = KABKODYA;
    }

    public String getPROVINCE() {
        return PROVINCE;
    }

    public void setPROVINCE(String PROVINCE) {
        this.PROVINCE = PROVINCE;
    }

    public String getPERSPOSTALCODE() {
        return PERSPOSTALCODE;
    }

    public void setPERSPOSTALCODE(String PERSPOSTALCODE) {
        this.PERSPOSTALCODE = PERSPOSTALCODE;
    }

    public String getSANDIDATIII() {
        return SANDIDATIII;
    }

    public void setSANDIDATIII(String SANDIDATIII) {
        this.SANDIDATIII = SANDIDATIII;
    }

    public String getHOME_STATUS() {
        return HOME_STATUS;
    }

    public void setHOME_STATUS(String HOME_STATUS) {
        this.HOME_STATUS = HOME_STATUS;
    }

    public String getTELEPHONE_AREA() {
        return TELEPHONE_AREA;
    }

    public void setTELEPHONE_AREA(String TELEPHONE_AREA) {
        this.TELEPHONE_AREA = TELEPHONE_AREA;
    }

    public String getTELEPHONE_NUMBER() {
        return TELEPHONE_NUMBER;
    }

    public void setTELEPHONE_NUMBER(String TELEPHONE_NUMBER) {
        this.TELEPHONE_NUMBER = TELEPHONE_NUMBER;
    }

    public String getHANDPHONE_PREFIX() {
        return HANDPHONE_PREFIX;
    }

    public void setHANDPHONE_PREFIX(String HANDPHONE_PREFIX) {
        this.HANDPHONE_PREFIX = HANDPHONE_PREFIX;
    }

    public String getHANDPHONE_NUMBER() {
        return HANDPHONE_NUMBER;
    }

    public void setHANDPHONE_NUMBER(String HANDPHONE_NUMBER) {
        this.HANDPHONE_NUMBER = HANDPHONE_NUMBER;
    }

    public String getOCCUPATION() {
        return OCCUPATION;
    }

    public void setOCCUPATION(String OCCUPATION) {
        this.OCCUPATION = OCCUPATION;
    }

    public String getJOBADDRESS() {
        return JOBADDRESS;
    }

    public void setJOBADDRESS(String JOBADDRESS) {
        this.JOBADDRESS = JOBADDRESS;
    }

    public String getJOBPOSTALCODE() {
        return JOBPOSTALCODE;
    }

    public void setJOBPOSTALCODE(String JOBPOSTALCODE) {
        this.JOBPOSTALCODE = JOBPOSTALCODE;
    }

    public String getLENGHOFWORK() {
        return LENGHOFWORK;
    }

    public void setLENGHOFWORK(String LENGHOFWORK) {
        this.LENGHOFWORK = LENGHOFWORK;
    }

    public String getFAM_IDENTIFY_TYPE() {
        return FAM_IDENTIFY_TYPE;
    }

    public void setFAM_IDENTIFY_TYPE(String FAM_IDENTIFY_TYPE) {
        this.FAM_IDENTIFY_TYPE = FAM_IDENTIFY_TYPE;
    }

    public String getFAM_BIRTH_DATE() {
        return FAM_BIRTH_DATE;
    }

    public void setFAM_BIRTH_DATE(String FAM_BIRTH_DATE) {
        this.FAM_BIRTH_DATE = FAM_BIRTH_DATE;
    }

    public String getGUAR_ADDRESS() {
        return GUAR_ADDRESS;
    }

    public void setGUAR_ADDRESS(String GUAR_ADDRESS) {
        this.GUAR_ADDRESS = GUAR_ADDRESS;
    }

    public String getGUAR_RELIGION() {
        return GUAR_RELIGION;
    }

    public void setGUAR_RELIGION(String GUAR_RELIGION) {
        this.GUAR_RELIGION = GUAR_RELIGION;
    }

    public String getPENJ_ADDRESS() {
        return PENJ_ADDRESS;
    }

    public void setPENJ_ADDRESS(String PENJ_ADDRESS) {
        this.PENJ_ADDRESS = PENJ_ADDRESS;
    }

    public String getGROSS_FIX_INCOME() {
        return GROSS_FIX_INCOME;
    }

    public void setGROSS_FIX_INCOME(String GROSS_FIX_INCOME) {
        this.GROSS_FIX_INCOME = GROSS_FIX_INCOME;
    }

    public String getADDITIONAL_INCOME() {
        return ADDITIONAL_INCOME;
    }

    public void setADDITIONAL_INCOME(String ADDITIONAL_INCOME) {
        this.ADDITIONAL_INCOME = ADDITIONAL_INCOME;
    }

    public String getPRODUCT_CODE() {
        return PRODUCT_CODE;
    }

    public void setPRODUCT_CODE(String PRODUCT_CODE) {
        this.PRODUCT_CODE = PRODUCT_CODE;
    }

    public String getDEALER_NAME() {
        return DEALER_NAME;
    }

    public void setDEALER_NAME(String DEALER_NAME) {
        this.DEALER_NAME = DEALER_NAME;
    }

    public String getBRANCH() {
        return BRANCH;
    }

    public void setBRANCH(String BRANCH) {
        this.BRANCH = BRANCH;
    }

    public String getCITY() {
        return CITY;
    }

    public void setCITY(String CITY) {
        this.CITY = CITY;
    }

    public String getBRAND() {
        return BRAND;
    }

    public void setBRAND(String BRAND) {
        this.BRAND = BRAND;
    }

    public String getMODEL() {
        return MODEL;
    }

    public void setMODEL(String MODEL) {
        this.MODEL = MODEL;
    }

    public String getTYPE() {
        return TYPE;
    }

    public void setTYPE(String TYPE) {
        this.TYPE = TYPE;
    }

    public String getYEAR() {
        return YEAR;
    }

    public void setYEAR(String YEAR) {
        this.YEAR = YEAR;
    }

    public String getDESCRIPTION() {
        return DESCRIPTION;
    }

    public void setDESCRIPTION(String DESCRIPTION) {
        this.DESCRIPTION = DESCRIPTION;
    }

    public String getORDER_DATE() {
        return ORDER_DATE;
    }

    public void setORDER_DATE(String ORDER_DATE) {
        this.ORDER_DATE = ORDER_DATE;
    }

    public String getCASH_PRICE() {
        return CASH_PRICE;
    }

    public void setCASH_PRICE(String CASH_PRICE) {
        this.CASH_PRICE = CASH_PRICE;
    }

    public String getTENOR() {
        return TENOR;
    }

    public void setTENOR(String TENOR) {
        this.TENOR = TENOR;
    }

    public String getJENIS_PEKERJAAN() {
        return JENIS_PEKERJAAN;
    }

    public void setJENIS_PEKERJAAN(String JENIS_PEKERJAAN) {
        this.JENIS_PEKERJAAN = JENIS_PEKERJAAN;
    }

    public String getPENGHASILAN_TETAP() {
        return PENGHASILAN_TETAP;
    }

    public void setPENGHASILAN_TETAP(String PENGHASILAN_TETAP) {
        this.PENGHASILAN_TETAP = PENGHASILAN_TETAP;
    }

    public String getADD_INCOME_SRC() {
        return ADD_INCOME_SRC;
    }

    public void setADD_INCOME_SRC(String ADD_INCOME_SRC) {
        this.ADD_INCOME_SRC = ADD_INCOME_SRC;
    }

    public String getFLAG_DELIVER() {
        return FLAG_DELIVER;
    }

    public void setFLAG_DELIVER(String FLAG_DELIVER) {
        this.FLAG_DELIVER = FLAG_DELIVER;
    }

    public String getFLAG_UPDATE_CLOSE() {
        return FLAG_UPDATE_CLOSE;
    }

    public void setFLAG_UPDATE_CLOSE(String FLAG_UPDATE_CLOSE) {
        this.FLAG_UPDATE_CLOSE = FLAG_UPDATE_CLOSE;
    }

    public String getNICKNAME() {
        return NICKNAME;
    }

    public void setNICKNAME(String NICKNAME) {
        this.NICKNAME = NICKNAME;
    }

    public String getMOTHER_NAME() {
        return MOTHER_NAME;
    }

    public void setMOTHER_NAME(String MOTHER_NAME) {
        this.MOTHER_NAME = MOTHER_NAME;
    }

    public String getFIRST_TITLE() {
        return FIRST_TITLE;
    }

    public void setFIRST_TITLE(String FIRST_TITLE) {
        this.FIRST_TITLE = FIRST_TITLE;
    }

    public String getGENDER() {
        return GENDER;
    }

    public void setGENDER(String GENDER) {
        this.GENDER = GENDER;
    }

    public String getBIRTH_PLACE() {
        return BIRTH_PLACE;
    }

    public void setBIRTH_PLACE(String BIRTH_PLACE) {
        this.BIRTH_PLACE = BIRTH_PLACE;
    }

    public String getDURATION_STAY_YEAR() {
        return DURATION_STAY_YEAR;
    }

    public void setDURATION_STAY_YEAR(String DURATION_STAY_YEAR) {
        this.DURATION_STAY_YEAR = DURATION_STAY_YEAR;
    }

    public String getDURATION_STAY_MONTH() {
        return DURATION_STAY_MONTH;
    }

    public void setDURATION_STAY_MONTH(String DURATION_STAY_MONTH) {
        this.DURATION_STAY_MONTH = DURATION_STAY_MONTH;
    }

    public String getINDUSTRY_CODE() {
        return INDUSTRY_CODE;
    }

    public void setINDUSTRY_CODE(String INDUSTRY_CODE) {
        this.INDUSTRY_CODE = INDUSTRY_CODE;
    }

    public String getOFFICE_NAME() {
        return OFFICE_NAME;
    }

    public void setOFFICE_NAME(String OFFICE_NAME) {
        this.OFFICE_NAME = OFFICE_NAME;
    }

    public String getTGL_DELIVERED() {
        return TGL_DELIVERED;
    }

    public void setTGL_DELIVERED(String TGL_DELIVERED) {
        this.TGL_DELIVERED = TGL_DELIVERED;
    }

    public String getTGL_CLOSE() {
        return TGL_CLOSE;
    }

    public void setTGL_CLOSE(String TGL_CLOSE) {
        this.TGL_CLOSE = TGL_CLOSE;
    }

    public String getTGL_CLOSE_SEND() {
        return TGL_CLOSE_SEND;
    }

    public void setTGL_CLOSE_SEND(String TGL_CLOSE_SEND) {
        this.TGL_CLOSE_SEND = TGL_CLOSE_SEND;
    }

    public String getMARITAL_STATUS() {
        return MARITAL_STATUS;
    }

    public void setMARITAL_STATUS(String MARITAL_STATUS) {
        this.MARITAL_STATUS = MARITAL_STATUS;
    }

    public String getOFFICE_PROVINCE() {
        return OFFICE_PROVINCE;
    }

    public void setOFFICE_PROVINCE(String OFFICE_PROVINCE) {
        this.OFFICE_PROVINCE = OFFICE_PROVINCE;
    }

    public String getOFFICE_CITY() {
        return OFFICE_CITY;
    }

    public void setOFFICE_CITY(String OFFICE_CITY) {
        this.OFFICE_CITY = OFFICE_CITY;
    }

    public String getCUSTOMER_EDUCATION() {
        return CUSTOMER_EDUCATION;
    }

    public void setCUSTOMER_EDUCATION(String CUSTOMER_EDUCATION) {
        this.CUSTOMER_EDUCATION = CUSTOMER_EDUCATION;
    }

    public String getDURATION_JOB_MONTH() {
        return DURATION_JOB_MONTH;
    }

    public void setDURATION_JOB_MONTH(String DURATION_JOB_MONTH) {
        this.DURATION_JOB_MONTH = DURATION_JOB_MONTH;
    }

    public String getCUSTOMER_RESI_ADDRESS() {
        return CUSTOMER_RESI_ADDRESS;
    }

    public void setCUSTOMER_RESI_ADDRESS(String CUSTOMER_RESI_ADDRESS) {
        this.CUSTOMER_RESI_ADDRESS = CUSTOMER_RESI_ADDRESS;
    }

    public String getCUSTOMER_RESI_RT() {
        return CUSTOMER_RESI_RT;
    }

    public void setCUSTOMER_RESI_RT(String CUSTOMER_RESI_RT) {
        this.CUSTOMER_RESI_RT = CUSTOMER_RESI_RT;
    }

    public String getCUSTOMER_RESI_RW() {
        return CUSTOMER_RESI_RW;
    }

    public void setCUSTOMER_RESI_RW(String CUSTOMER_RESI_RW) {
        this.CUSTOMER_RESI_RW = CUSTOMER_RESI_RW;
    }

    public String getCUSTOMER_RESI_VILLAGE() {
        return CUSTOMER_RESI_VILLAGE;
    }

    public void setCUSTOMER_RESI_VILLAGE(String CUSTOMER_RESI_VILLAGE) {
        this.CUSTOMER_RESI_VILLAGE = CUSTOMER_RESI_VILLAGE;
    }

    public String getCUSTOMER_RESI_DISTRICT() {
        return CUSTOMER_RESI_DISTRICT;
    }

    public void setCUSTOMER_RESI_DISTRICT(String CUSTOMER_RESI_DISTRICT) {
        this.CUSTOMER_RESI_DISTRICT = CUSTOMER_RESI_DISTRICT;
    }

    public String getCUSTOMER_RESI_REGENCY() {
        return CUSTOMER_RESI_REGENCY;
    }

    public void setCUSTOMER_RESI_REGENCY(String CUSTOMER_RESI_REGENCY) {
        this.CUSTOMER_RESI_REGENCY = CUSTOMER_RESI_REGENCY;
    }

    public String getCUSTOMER_RESI_PROVINCE() {
        return CUSTOMER_RESI_PROVINCE;
    }

    public void setCUSTOMER_RESI_PROVINCE(String CUSTOMER_RESI_PROVINCE) {
        this.CUSTOMER_RESI_PROVINCE = CUSTOMER_RESI_PROVINCE;
    }

    public String getCUSTOMER_RESI_POSTCODE() {
        return CUSTOMER_RESI_POSTCODE;
    }

    public void setCUSTOMER_RESI_POSTCODE(String CUSTOMER_RESI_POSTCODE) {
        this.CUSTOMER_RESI_POSTCODE = CUSTOMER_RESI_POSTCODE;
    }

    public String getCUSTOMER_RESI_CITY() {
        return CUSTOMER_RESI_CITY;
    }

    public void setCUSTOMER_RESI_CITY(String CUSTOMER_RESI_CITY) {
        this.CUSTOMER_RESI_CITY = CUSTOMER_RESI_CITY;
    }

    public String getDOWNPAYMENT_PERC() {
        return DOWNPAYMENT_PERC;
    }

    public void setDOWNPAYMENT_PERC(String DOWNPAYMENT_PERC) {
        this.DOWNPAYMENT_PERC = DOWNPAYMENT_PERC;
    }

    public String getDOWNPAYMENT_AMT() {
        return DOWNPAYMENT_AMT;
    }

    public void setDOWNPAYMENT_AMT(String DOWNPAYMENT_AMT) {
        this.DOWNPAYMENT_AMT = DOWNPAYMENT_AMT;
    }

    public String getINSTALLMENT_AMT() {
        return INSTALLMENT_AMT;
    }

    public void setINSTALLMENT_AMT(String INSTALLMENT_AMT) {
        this.INSTALLMENT_AMT = INSTALLMENT_AMT;
    }

    public String getINSTALLMENT_TYPE() {
        return INSTALLMENT_TYPE;
    }

    public void setINSTALLMENT_TYPE(String INSTALLMENT_TYPE) {
        this.INSTALLMENT_TYPE = INSTALLMENT_TYPE;
    }

    public String getRENT_PAYMENT_TYPE() {
        return RENT_PAYMENT_TYPE;
    }

    public void setRENT_PAYMENT_TYPE(String RENT_PAYMENT_TYPE) {
        this.RENT_PAYMENT_TYPE = RENT_PAYMENT_TYPE;
    }

    public String getASSET_NO() {
        return ASSET_NO;
    }

    public void setASSET_NO(String ASSET_NO) {
        this.ASSET_NO = ASSET_NO;
    }

    public String getADD_INCOME_SOURCE() {
        return ADD_INCOME_SOURCE;
    }

    public void setADD_INCOME_SOURCE(String ADD_INCOME_SOURCE) {
        this.ADD_INCOME_SOURCE = ADD_INCOME_SOURCE;
    }

    public String getSPOUSE_NAME() {
        return SPOUSE_NAME;
    }

    public void setSPOUSE_NAME(String SPOUSE_NAME) {
        this.SPOUSE_NAME = SPOUSE_NAME;
    }

    public String getSPOUSE_ID_TYPE() {
        return SPOUSE_ID_TYPE;
    }

    public void setSPOUSE_ID_TYPE(String SPOUSE_ID_TYPE) {
        this.SPOUSE_ID_TYPE = SPOUSE_ID_TYPE;
    }

    public String getSPOUSE_ID_NO() {
        return SPOUSE_ID_NO;
    }

    public void setSPOUSE_ID_NO(String SPOUSE_ID_NO) {
        this.SPOUSE_ID_NO = SPOUSE_ID_NO;
    }

    public String getCHILD_NAME() {
        return CHILD_NAME;
    }

    public void setCHILD_NAME(String CHILD_NAME) {
        this.CHILD_NAME = CHILD_NAME;
    }

    public String getCHILD_SCHOOL_NAME() {
        return CHILD_SCHOOL_NAME;
    }

    public void setCHILD_SCHOOL_NAME(String CHILD_SCHOOL_NAME) {
        this.CHILD_SCHOOL_NAME = CHILD_SCHOOL_NAME;
    }

    public String getCHILD_SCHOOL_ADDRESS() {
        return CHILD_SCHOOL_ADDRESS;
    }

    public void setCHILD_SCHOOL_ADDRESS(String CHILD_SCHOOL_ADDRESS) {
        this.CHILD_SCHOOL_ADDRESS = CHILD_SCHOOL_ADDRESS;
    }

    public String getSAME_CUSTOMER_ID_ADDRESS() {
        return SAME_CUSTOMER_ID_ADDRESS;
    }

    public void setSAME_CUSTOMER_ID_ADDRESS(String SAME_CUSTOMER_ID_ADDRESS) {
        this.SAME_CUSTOMER_ID_ADDRESS = SAME_CUSTOMER_ID_ADDRESS;
    }

    public String getASSET_CONDITION() {
        return ASSET_CONDITION;
    }

    public void setASSET_CONDITION(String ASSET_CONDITION) {
        this.ASSET_CONDITION = ASSET_CONDITION;
    }

    public String getINVOICE_CORRESPONDENCE_NO() {
        return INVOICE_CORRESPONDENCE_NO;
    }

    public void setINVOICE_CORRESPONDENCE_NO(String INVOICE_CORRESPONDENCE_NO) {
        this.INVOICE_CORRESPONDENCE_NO = INVOICE_CORRESPONDENCE_NO;
    }

    private String DATA_MAP_NAME;
    private String ORDERID;
    private String MODULE_ID;
    private String ITEM_TYPE;
    private String ITEM_KEY;
    private String USER_ID;
    private String STATUS;
    private String STATUS_UPDATE;
    private String TIMESTAMPS;
    private String LAST_TRX;
    private String BRANCH_ID;
    private String STATUS_KUNJUNGAN;
    private String BLANK;
    private String NAME;
    private String PERS_IDENTIFY_TYPE;
    private String IDENTIFY_NO;
    private String NPWP_NO;
    private String CUST_BIRTH_DATE;
    private String PERS_RELIGION;
    private String ADDRESS_KTP;
    private String RT;
    private String RW;
    private String KELURAHAN;
    private String KECAMATAN;
    private String KABKODYA;
    private String PROVINCE;
    private String PERSPOSTALCODE;
    private String SANDIDATIII;
    private String HOME_STATUS;
    private String TELEPHONE_AREA;
    private String TELEPHONE_NUMBER;
    private String HANDPHONE_PREFIX;
    private String HANDPHONE_NUMBER;
    private String OCCUPATION;
    private String JOBADDRESS;
    private String JOBPOSTALCODE;
    private String LENGHOFWORK;
    private String FAM_IDENTIFY_TYPE;
    private String FAM_BIRTH_DATE;
    private String GUAR_ADDRESS;
    private String GUAR_RELIGION;
    private String PENJ_ADDRESS;
    private String GROSS_FIX_INCOME;
    private String ADDITIONAL_INCOME;
    private String PRODUCT_CODE;
    private String DEALER_NAME;
    private String BRANCH;
    private String CITY;
    private String BRAND;
    private String MODEL;
    private String TYPE;
    private String YEAR;
    private String DESCRIPTION;
    private String ORDER_DATE;
    private String CASH_PRICE;
    private String TENOR;
    private String JENIS_PEKERJAAN;
    private String PENGHASILAN_TETAP;
    private String ADD_INCOME_SRC;
    private String FLAG_DELIVER;
    private String FLAG_UPDATE_CLOSE;
    private String NICKNAME;
    private String MOTHER_NAME;
    private String FIRST_TITLE;
    private String GENDER;
    private String BIRTH_PLACE;
    private String DURATION_STAY_YEAR;
    private String DURATION_STAY_MONTH;
    private String INDUSTRY_CODE;
    private String OFFICE_NAME;
    private String TGL_DELIVERED;
    private String TGL_CLOSE;
    private String TGL_CLOSE_SEND;
    private String MARITAL_STATUS;
    private String OFFICE_PROVINCE;
    private String OFFICE_CITY;
    private String CUSTOMER_EDUCATION;
    private String DURATION_JOB_MONTH;
    private String CUSTOMER_RESI_ADDRESS;
    private String CUSTOMER_RESI_RT;
    private String CUSTOMER_RESI_RW;
    private String CUSTOMER_RESI_VILLAGE;
    private String CUSTOMER_RESI_DISTRICT;
    private String CUSTOMER_RESI_REGENCY;
    private String CUSTOMER_RESI_PROVINCE;
    private String CUSTOMER_RESI_POSTCODE;
    private String CUSTOMER_RESI_CITY;
    private String DOWNPAYMENT_PERC;
    private String DOWNPAYMENT_AMT;
    private String INSTALLMENT_AMT;
    private String INSTALLMENT_TYPE;
    private String RENT_PAYMENT_TYPE;
    private String ASSET_NO;
    private String ADD_INCOME_SOURCE;
    private String SPOUSE_NAME;
    private String SPOUSE_ID_TYPE;
    private String SPOUSE_ID_NO;
    private String CHILD_NAME;
    private String CHILD_SCHOOL_NAME;
    private String CHILD_SCHOOL_ADDRESS;
    private String SAME_CUSTOMER_ID_ADDRESS;
    private String ASSET_CONDITION;
    private String INVOICE_CORRESPONDENCE_NO;
}

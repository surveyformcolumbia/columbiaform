package com.indocyber.columbia.Model;

import io.realm.RealmObject;

/**
 * Created by Indocyber on 17/07/2017.
 */

public class ProductCondition extends RealmObject {
    private String PRODUCT_CONDITION_ID;
    private String PRODUCT_CONDITION_NAME;
    private String ACTIVE_STATUS;
    private String CREATED_BY;
    private String CREATED_DATE;
    private String MODIFIED_BY;
    private String MODIFIED_DATE;
    public String getPRODUCT_CONDITION_ID() {
        return PRODUCT_CONDITION_ID;
    }

    public void setPRODUCT_CONDITION_ID(String PRODUCT_CONDITION_ID) {
        this.PRODUCT_CONDITION_ID = PRODUCT_CONDITION_ID;
    }

    public String getPRODUCT_CONDITION_NAME() {
        return PRODUCT_CONDITION_NAME;
    }

    public void setPRODUCT_CONDITION_NAME(String PRODUCT_CONDITION_NAME) {
        this.PRODUCT_CONDITION_NAME = PRODUCT_CONDITION_NAME;
    }

    public String getACTIVE_STATUS() {
        return ACTIVE_STATUS;
    }

    public void setACTIVE_STATUS(String ACTIVE_STATUS) {
        this.ACTIVE_STATUS = ACTIVE_STATUS;
    }

    public String getCREATED_BY() {
        return CREATED_BY;
    }

    public void setCREATED_BY(String CREATED_BY) {
        this.CREATED_BY = CREATED_BY;
    }

    public String getCREATED_DATE() {
        return CREATED_DATE;
    }

    public void setCREATED_DATE(String CREATED_DATE) {
        this.CREATED_DATE = CREATED_DATE;
    }

    public String getMODIFIED_BY() {
        return MODIFIED_BY;
    }

    public void setMODIFIED_BY(String MODIFIED_BY) {
        this.MODIFIED_BY = MODIFIED_BY;
    }

    public String getMODIFIED_DATE() {
        return MODIFIED_DATE;
    }

    public void setMODIFIED_DATE(String MODIFIED_DATE) {
        this.MODIFIED_DATE = MODIFIED_DATE;
    }
}

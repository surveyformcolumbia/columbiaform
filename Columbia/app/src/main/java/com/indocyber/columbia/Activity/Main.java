package com.indocyber.columbia.Activity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.indocyber.columbia.Abstract.AppActivity;
import com.indocyber.columbia.Activity.NewOrder.NewOrderActivity;
import com.indocyber.columbia.Activity.NewOrder.NewOrderList;
import com.indocyber.columbia.R;

import org.json.JSONException;

public class Main extends AppActivity implements View.OnClickListener {
    private TextView txtLoginAs;
    private LinearLayout menuNewOrder,menuSavedOrder,menuSentOrder,menuPendingOrder,menuAccount,menuLogout;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        /*
        init component
         */
        txtLoginAs= (TextView) findViewById(R.id.textViewLoginAs);
        menuAccount= (LinearLayout) findViewById(R.id.menuAccount);
        menuNewOrder= (LinearLayout) findViewById(R.id.menuNewOrder);
        menuSavedOrder= (LinearLayout) findViewById(R.id.menuSavedOrder);
        menuPendingOrder= (LinearLayout) findViewById(R.id.menuPendingOrder);
        menuSentOrder= (LinearLayout) findViewById(R.id.menuSentOrder);
        menuLogout= (LinearLayout) findViewById(R.id.menuLogout);
        /*
        set listener click here
         */
        menuAccount.setOnClickListener(this);
        menuNewOrder.setOnClickListener(this);
        menuPendingOrder.setOnClickListener(this);
        menuSavedOrder.setOnClickListener(this);
        menuSentOrder.setOnClickListener(this);
        menuLogout.setOnClickListener(this);
        setTitle();
    }
    @Override
    protected void onResume() {
        super.onResume();
        setTitle();
    }
    public void setTitle(){
         /*
        set Login Ass
         */
        try {
            txtLoginAs.setText("You are login as "+session.getUserdata().getString("Fullname"));
        } catch (JSONException e) {
            Log.e(getClass().getSimpleName(),e.getMessage());
        }
    }
    /*
    all action click here guys
     */
    @Override
    public void onClick(View v) {
        //choose action by element
        switch (v.getId()){
            case R.id.menuAccount :
                /*
                go to user settings
                 */
                Intent userConfig=new Intent(this,UserSettings.class);
                startActivity(userConfig);
                break;
            case R.id.menuLogout :
                /*
                logout User
                 */
                session.setLogin(false);
                session.setSynced(false);
                finish();
                break;
            case R.id.menuNewOrder :
                /*
                go to new order page
                 */
                Intent newOrder=new Intent(this, NewOrderList.class);
                startActivity(newOrder);
                break;
            case R.id.menuPendingOrder :
                /*
                go to pending order page
                 */
                Intent pendingOrder=new Intent(this, PendingOrder.class);
                startActivity(pendingOrder);
                break;
            case R.id.menuSentOrder :
                /*
                go to sent order page
                 */
                Intent sendtOrder=new Intent(this,SentOrder.class);
                startActivity(sendtOrder);
                break;
            case R.id.menuSavedOrder :
                /*
                go to save order page
                 */
                Intent saveOrder=new Intent(this,SavedOrder.class);
                startActivity(saveOrder);
                break;
        }
    }
}

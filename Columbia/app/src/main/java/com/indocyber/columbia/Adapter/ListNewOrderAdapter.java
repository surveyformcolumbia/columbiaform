package com.indocyber.columbia.Adapter;

import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.indocyber.columbia.Model.ItemNewOrder;
import com.indocyber.columbia.R;

import java.util.List;

/**
 * Created by Frendi on 20/07/2017.
 */

public class ListNewOrderAdapter extends RecyclerView.Adapter<ListNewOrderAdapter.ViewHolder>{

    private List<ItemNewOrder> item;
    private OnListItemClick onListItemClick;

    public ListNewOrderAdapter (List<ItemNewOrder> item){
        this.item = item;
    }

    public void setOnListItemClick (OnListItemClick onListItemClick){
        this.onListItemClick = onListItemClick;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.custom_neworder, null);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        ItemNewOrder model = item.get(position);
        holder.orderId.setText(model.orderId);
        holder.name.setText(model.nama);
        holder.address.setText(model.alamat);
        holder.handphone.setText(model.handphone);
    }

    @Override
    public int getItemCount() {
        return item.size();
    }

    public interface OnListItemClick {
        void onItemClick(View v, int position, ItemNewOrder model);
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        public TextView orderId, name, address, handphone;
        public LinearLayout mainContainer;
        public CardView cardView;

        public ViewHolder(View itemView) {
            super(itemView);
            cardView = (CardView) itemView;
            mainContainer = (LinearLayout) cardView.findViewById(R.id.itemNewOrder_container);
            orderId = (TextView) itemView.findViewById(R.id.item_orderId);
            name = (TextView) itemView.findViewById(R.id.item_nama);
            address = (TextView) itemView.findViewById(R.id.item_alamat);
            handphone = (TextView) itemView.findViewById(R.id.item_handphone);
            mainContainer.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (onListItemClick != null) {
                        final int position = getAdapterPosition();
                        onListItemClick.onItemClick(mainContainer, position, item.get(position));
                    }
                }
            });
        }
    }

}

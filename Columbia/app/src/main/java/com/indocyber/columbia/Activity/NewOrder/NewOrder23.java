package com.indocyber.columbia.Activity.NewOrder;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.indocyber.columbia.Contract.NewOrderInterface;
import com.indocyber.columbia.Model.OrderEntry;
import com.indocyber.columbia.R;

public class NewOrder23 extends Fragment {

    private static final String TAG = "NewOrder23";
    private NewOrderInterface newOrderInterface;
    private OrderEntry orderEntry = null;

    public static NewOrder23 newInstance() {
        return new NewOrder23();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.d(TAG, "onCreate: ");
        orderEntry = newOrderInterface.getOrderEntry();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_new_order23, container, false);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        Log.d(TAG, "onAttach: ");
        try {
            newOrderInterface = (NewOrderInterface) context;
        }catch (Exception e){
            e.printStackTrace();
        }
    }

}

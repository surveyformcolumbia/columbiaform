package com.indocyber.columbia.Model;

import io.realm.RealmObject;

/**
 * Created by Indocyber on 17/07/2017.
 */

public class Combo extends RealmObject {
    private String ComboxID;

    public String getComboxID() {
        return ComboxID;
    }

    public void setComboxID(String comboxID) {
        ComboxID = comboxID;
    }

    public String getLabel() {
        return Label;
    }

    public void setLabel(String label) {
        Label = label;
    }

    public String getValueList() {
        return ValueList;
    }

    public void setValueList(String valueList) {
        ValueList = valueList;
    }

    private String Label;
    private String ValueList;
}
